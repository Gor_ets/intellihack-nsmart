import json


class Location:

    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude


class BusStop:
    def __init__(self, uuid, name, location: 'Location'):
        self.uuid: int = uuid
        self.name: str = name
        self.location = location

    def get_latitude(self):
        return self.location.latitude

    def get_longitude(self):
        return self.location.longitude

    @staticmethod
    def bus_stops_from_json(path: str) -> list['BusStop']:
        result: list[BusStop] = list()
        with open(path, 'r') as file:
            data = json.load(file)
            for stop in data:
                latitude = stop.get('stations_gpsx')
                longitude = stop.get('stations_gpsy')
                # skip stops with missing coordinates
                if latitude is None or longitude is None:
                    continue

                result.append(BusStop(
                    stop['id'],
                    stop['bus_stop_name'],
                    Location(latitude, longitude)
                ))
            return result


class BusStopDistance:

    def __init__(self, stop: BusStop, distance: float):
        self.stop: BusStop = stop
        self.distance: float = distance


class Announcement:

    def __init__(self, station_name: str, line_number: str, main_line_title: str, seconds_left: int):
        self.station_name: str = station_name
        self.line_number: str = line_number
        self.main_line_title: str = main_line_title
        self.seconds_left: int = seconds_left

    def minutes_left(self) -> int:
        return int(self.seconds_left % 86400 % 3600 / 60)

    @staticmethod
    def from_json(data) -> list['Announcement']:
        result: list['Announcement'] = []
        for el in data:
            # skip announcements w/o coordinates or required fields
            if el.get('just_coordinates') == "1" or not all(key in el for key in [
                "line_number", "line_title", "main_line_title", "seconds_left", "station_name"
            ]):
                continue

            result.append(
                Announcement(el['station_name'], el['line_number'], el['main_line_title'], el['seconds_left']))

        return result
