# Deprecated

Converted to [project in Web](https://gitlab.com/Gor_ets/public-transport-in-serbia)

## IntelliHack NSmart
Reverse engineering NSmart APP and API

[NSmart app in Google Play](https://play.google.com/store/apps/details?id=buslogic.nsmartapp&hl=en&gl=US)

## Telegram Bot 

Implemented in telegram bot - https://t.me/novisad_transport_bot

## Update locations of bus stops in novisad_bus_stops.json

```
python3 json/add_locations_to_bus_stops.py
```

## API (raw)

Get all bus stops from Novi Sad and areas around:
```
curl --location --request GET 'https://online.nsmart.rs/publicapi/v1/networkextended.php?action=get_cities_extended' --header 'X-Api-Authentication: 4670f468049bbee2260'
```

From the [response JSON](json/raw_bus_stops_all_cities.json) we extract station information. For instance, ID for station "Temerinska-Najlon pijaca" is `6625`.

Now we can form Announcment API call for this station:
```
curl --location --request GET 'https://online.nsmart.rs/publicapi/v1/announcement/announcement.php?station_uid=6625' --header 'X-Api-Authentication: 4670f468049bbee2260'
```
The [JSON response](json/announcment_6625.json), among other things, gives us the exact current position for all busses that stop at the given `6625` bus station.

**The API key is 4670f468049bbee2260.**

## Analyzed info

All bus stops - [response JSON](json/novisad_bus_stops.json). There are stops with unique names and locations.