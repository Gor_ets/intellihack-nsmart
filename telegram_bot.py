import asyncio
import logging
import os
from datetime import datetime, timedelta

import pytz
from aiogram import Bot, Dispatcher, types
from aiogram.filters.command import Command
from aiogram.types import Location

import nsmart
from model import BusStop, Location, Announcement

logging.basicConfig(level=logging.INFO)
bot = Bot(token=os.getenv('TELEGRAM_BOT_TOKEN'))
dp = Dispatcher()
refresh_cooldown: dict[int, datetime] = {}  # userid: cooldown mapping


@dp.message(Command("start"))
async def cmd_start(message: types.Message):
    await message.reply("Hi! I am bot for finding routes to bus stops. Give me location to start search.")


@dp.message(Command("help"))
async def cmd_help(message: types.Message):
    await message.reply("How to use bot?\n\n"
                        "Send your location, select bus stop and you will see buses that are coming to that stop.\n")


@dp.message(Command("share_my_location"))
async def cmd_share_my_location(message: types.Message):
    kb = [
        [types.KeyboardButton(text="Send Location", request_location=True)],
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
    await message.reply("Please share your location", reply_markup=keyboard)


async def send_location(chat_id: int, latitude: float, longitude: float, stop: BusStop | None = None):
    if stop is None:
        await bot.send_location(chat_id, latitude, longitude)
        return
    button = types.InlineKeyboardButton(text=stop.name, callback_data=f"id_{stop.uuid}")
    markup = types.InlineKeyboardMarkup(inline_keyboard=[[button]])
    await bot.send_location(chat_id, latitude, longitude, reply_markup=markup)


@dp.message(lambda message: message.content_type == types.ContentType.LOCATION)
async def handle_location(message: types.Message):
    user_location = Location(message.location.latitude, message.location.longitude)
    nearest_stops = nsmart.find_nearest_stops(user_location, 5)
    if not nearest_stops:
        await message.reply("No nearby bus stops found.")
        return

    await bot.send_message(message.chat.id, "Select bus stop:")
    for stop_distance in nearest_stops:
        stop = stop_distance.stop
        await bot.send_message(message.chat.id, f"{stop.name} - {stop_distance.distance:.2f} km away")
        await send_location(message.chat.id, stop.location.latitude, stop.location.longitude, stop)


@dp.callback_query(lambda call: call.data.startswith("id_"))
async def handle_callback_query(call: types.CallbackQuery):
    stop_id = call.data.split("_")[1]
    try:
        announcement_data = await parse_announcements(nsmart.get_announcement_data(stop_id))

        refresh_button = types.InlineKeyboardButton(text="Refresh", callback_data=f"refresh_{stop_id}")
        keyboard = types.InlineKeyboardMarkup(inline_keyboard=[[refresh_button]])

        await bot.send_message(call.message.chat.id, announcement_data, reply_markup=keyboard)

    except Exception as e:
        error = "Unknown Error: " + str(e)
        await bot.send_message(call.message.chat.id, error)
    finally:
        await bot.answer_callback_query(call.id)


@dp.callback_query(lambda call: call.data.startswith("refresh_"))
async def handle_refresh_query(call: types.CallbackQuery):
    user_id = call.from_user.id
    stop_id = call.data.split("_")[1]

    if user_id in refresh_cooldown:
        last_refresh_time = refresh_cooldown[user_id]
        if (datetime.now() - last_refresh_time) < timedelta(seconds=3):
            return

    try:
        announcement_data = await parse_announcements(nsmart.get_announcement_data(stop_id))

        refresh_button = types.InlineKeyboardButton(text="Refresh", callback_data=f"refresh_{stop_id}")
        keyboard = types.InlineKeyboardMarkup(inline_keyboard=[[refresh_button]])

        belgrade_tz = pytz.timezone('Europe/Belgrade')
        current_time_belgrade = datetime.now(belgrade_tz).strftime("%H:%M:%S %d.%m.%Y")
        updated_message_text = f"{announcement_data}\nLast refreshed (Serbia): {current_time_belgrade}"

        await bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text=updated_message_text,
            reply_markup=keyboard
        )

        refresh_cooldown[user_id] = datetime.now()

    except Exception as e:
        error = "Unknown Error: " + str(e)
        await bot.send_message(call.message.chat.id, error)


async def parse_announcements(announcements: list[Announcement]) -> str:
    if len(announcements) == 0:
        return "No complete bus route information available for this stop."

    station_name = announcements[0].station_name
    announcement_format = (
        f"== {station_name} ==\n\n"
    )

    result = [announcement_format]

    for a in announcements:
        eta = (
            "Bus should already be at stop" if a.minutes_left() == 0
            else f"Bus will be here in: {a.minutes_left()} minutes"
        )
        announcement = (
            f"Bus: {a.line_number} number\n"
            f"Route: {a.main_line_title}\n"
            f"{eta}\n\n"
        )
        result.append(announcement)

    return ''.join(result)


async def main():
    try:
        await dp.start_polling(bot)
    except Exception as e:
        logging.exception("Error occurred while bot was running: %s", e)
        await asyncio.sleep(10)
        await asyncio.create_task(main())


if __name__ == "__main__":
    asyncio.run(main())
