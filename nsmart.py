import requests

from geopy.distance import geodesic
from model import BusStop, Location, BusStopDistance, Announcement

TRANSLITERATION_DICT: dict[str, list[str]] = {
    'c': ['c', 'č', 'ć'],
    's': ['s', 'š'],
    'z': ['z', 'ž'],
    'd': ['d', 'đ']
}

NOVISAD_BUS_STOPS: list[BusStop] = BusStop.bus_stops_from_json('json/novisad_bus_stops.json')
NSMART_API_AUTH_HEADERS: dict[str, str] = {'X-Api-Authentication': '4670f468049bbee2260'}


def generate_transliterated_names(name: str) -> list[str]:
    """
    Generate all variants of given string, replacing latin characters with similar Serbian ones
    :param name: input string
    :return: list of all variants of this string
    """
    if not name:
        return [name]
    transliterated_names = ['']
    for char in name.lower():
        possible_chars = TRANSLITERATION_DICT.get(char, [char])
        transliterated_names = [name_variant + ch for name_variant in transliterated_names for ch in possible_chars]
    return transliterated_names


def get_announcement_data(station_uid) -> list[Announcement]:
    url = f"https://online.nsmart.rs/publicapi/v1/announcement/announcement.php?station_uid={station_uid}"
    response = requests.get(url, headers=NSMART_API_AUTH_HEADERS)

    if response.status_code != 200:
        raise Exception("Failed to load announcement data.")

    return sorted(Announcement.from_json(response.json()), key=lambda a: a.seconds_left)


def find_nearest_stops(user_location: Location, num_stops: int) -> list[BusStopDistance]:
    """
    Find nearest num_stops stops to the given user_location
    :param user_location: user's location
    :param num_stops: maximum amount of stops to return
    :return: List of BusStop object, sorted by distance to user_location
    """

    result: list[BusStopDistance] = list()

    for stop in NOVISAD_BUS_STOPS:
        distance = geodesic(
            (user_location.latitude, user_location.longitude),
            (stop.get_latitude(), stop.get_longitude())
        ).kilometers

        result.append(BusStopDistance(stop, distance))

    return sorted(result, key=lambda s: s.distance)[:num_stops]
