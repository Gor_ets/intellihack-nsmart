import json
import logging
import os
import sys

import requests

# Setup logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s: %(message)s')
console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)

current_directory = os.path.dirname(__file__)
file_path = os.path.join(current_directory, 'novisad_bus_stops.json')
headers = {'X-Api-Authentication': '4670f468049bbee2260'}

with open(file_path, 'r') as file:
    bus_stops = json.load(file)

for stop in bus_stops:
    if stop['stations_gpsx'] is None and stop['stations_gpsy'] is None:
        try:
            response = requests.get(
                f"https://online.nsmart.rs/publicapi/v1/announcement/announcement.php?station_uid={stop['id']}",
                headers=headers
            )
            if response.status_code == 200:
                data = response.json()
                if 'stations_gpsx' in data[0] and 'stations_gpsy' in data[0]:
                    stop['stations_gpsx'] = data[0]['stations_gpsx']
                    stop['stations_gpsy'] = data[0]['stations_gpsy']
            else:
                logger.error(f"Error with station ID {stop['id']}: {response.status_code}")
        except json.JSONDecodeError:
            logger.error(f"Invalid JSON response for station ID {stop['id']}")
        except requests.RequestException as e:
            logger.error(f"Request error for station ID {stop['id']}: {e}")

with open(file_path, 'w', encoding='utf-8') as file:
    logger.info(f"{file_path} updated")
    json.dump(bus_stops, file, indent=4, ensure_ascii=False)
